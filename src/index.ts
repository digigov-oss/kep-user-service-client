import {
    generateAuditRecord,
    AuditRecord,
    FileEngine,
    AuditEngine,
} from '@digigov-oss/gsis-audit-record-db';
import { RestClient } from './restClient.js';
import config from './config.json';
import {
    GetKepUserRegAllEmployeesResponse,
    GetKepUserRegEmployeeResponse,
    KedResponseWithAuditRecord,
} from './interface';

export * from './interface';

/**
 * @type Overrides
 * @description Overrides for the REST client
 * @param {string} endpoint - Endpoint to be used for the REST client
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 * @param {AuditEngine} auditEngine - Audit engine to be used for the audit record produced
 */
export type Overrides = {
    endpoint?: string;
    prod?: boolean;
    auditInit?: AuditRecord;
    auditStoragePath?: string;
    auditEngine?: AuditEngine;
};

/**
 *
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 */
class KepUserServiceClient {
    endpoint: string;
    prod: boolean;
    auditInit: object;
    auditStoragePath: string;
    auditEngine: AuditEngine;
    user: string;
    pass: string;

    constructor(user: string, pass: string, overrides?: Overrides) {
        this.prod = overrides?.prod ?? false;
        if (overrides?.endpoint) {
            this.endpoint = overrides?.endpoint;
        } else {
            this.endpoint = this.prod ? config.prod : config.test;
        }
        this.auditInit = overrides?.auditInit ?? ({} as AuditRecord);
        this.auditStoragePath = overrides?.auditStoragePath ?? '/tmp';
        this.auditEngine =
            overrides?.auditEngine ?? new FileEngine(this.auditStoragePath);
        this.user = user;
        this.pass = pass;
    }

    genAuditRecord = async (overrides?: Overrides): Promise<AuditRecord> => {
        const auditInit = Object.assign(
            {},
            this.auditInit,
            overrides?.auditInit,
        );
        const auditRecord = await generateAuditRecord(
            auditInit,
            this.auditEngine,
        );
        if (!auditRecord) throw new Error('Audit record is not initialized');
        return auditRecord;
    };

    public getEmployee = async (
        afm: string,
        overrides?: Overrides,
    ): Promise<KedResponseWithAuditRecord<GetKepUserRegEmployeeResponse>> => {
        const auditRecord = await this.genAuditRecord(overrides);
        const restClient = new RestClient(this.user, this.pass, this.endpoint);
        const resp = await restClient.getEmployee(afm, auditRecord);

        return {
            kedResponse: resp,
            auditRecord: auditRecord,
        };
    };

    public getAllEmployees = async (
        kepcode: string,
        overrides?: Overrides,
    ): Promise<
        KedResponseWithAuditRecord<GetKepUserRegAllEmployeesResponse>
    > => {
        const auditRecord = await this.genAuditRecord(overrides);
        const restClient = new RestClient(this.user, this.pass, this.endpoint);
        const resp = await restClient.getAllEmployees(kepcode, auditRecord);
        return {
            kedResponse: resp,
            auditRecord: auditRecord,
        };
    };
}

export default KepUserServiceClient;
