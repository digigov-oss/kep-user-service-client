import { AuditRecord } from '@digigov-oss/gsis-audit-record-db';
import {
    GetKepUserRegEmployeeRequest,
    GetKepUserRegEmployeeResponse,
    GetKepUserRegAllEmployeesRequest,
    GetKepUserRegAllEmployeesResponse,
} from './interface';

import axios from 'axios';

export class RestClient {
    user: string;
    pass: string;
    endpoint: string;

    constructor(user: string, pass: string, endpoint: string) {
        this.user = user;
        this.pass = pass;
        this.endpoint = endpoint;
    }

    getEmployee = async (afm: string, auditRecord: AuditRecord) => {
        const request: GetKepUserRegEmployeeRequest = {
            auditRecord: auditRecord,
            getKepUserRegEmployeeInputRecord: {
                afm,
            },
        };
        const options = {
            auth: {
                username: this.user,
                password: this.pass,
            },
        };
        const ed = `${this.endpoint}/getKepUserRegEmployee`;
        const response = await axios.post(ed, request, options);
        return response.data as GetKepUserRegEmployeeResponse;
    };

    getAllEmployees = async (kepCode: string, auditRecord: AuditRecord) => {
        const request: GetKepUserRegAllEmployeesRequest = {
            auditRecord: auditRecord,
            getKepUserRegAllEmployeesInputRecord: {
                kepCode,
            },
        };
        const options = {
            auth: {
                username: this.user,
                password: this.pass,
            },
        };
        const ed = `${this.endpoint}/getKepUserRegAllEmployees`;
        const response = await axios.post(ed, request, options);
        return response.data as GetKepUserRegAllEmployeesResponse;
    };
}
