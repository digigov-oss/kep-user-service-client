import KepUserServiceClient from '../src/index';
import config from './config.json';
import inspect from 'object-inspect';

const test = async () => {
    // create a new client instance
    const serviceClient = new KepUserServiceClient(config.user, config.pass, config.overrides);

    console.log(
        inspect(
            await serviceClient.getEmployee("000000000"),
            { depth: 10 },
        ),
    );

    console.log(
        inspect(
            await serviceClient.getAllEmployees("0699").catch(e => { 
                console.log(e);
            }),
            { depth: 10 },
        ),
    );
};

test();

