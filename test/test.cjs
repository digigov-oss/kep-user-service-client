const KepUserServiceClient = require('../dist/cjs/index.js').default;
const config = require('./config.json'); 
const inspect = require('object-inspect');

const test = async () => {
    const serviceClient = new KepUserServiceClient(
        config.user,
        config.pass,
        config.overrides
    );

   console.log(
        inspect(
            await serviceClient.getEmployee("051655852"),
            { depth: 10 },
        ),
    );

    console.log(
        inspect(
            await serviceClient.getAllEmployees("0699"),
            { depth: 10 },
        ),
    );
};

test();
