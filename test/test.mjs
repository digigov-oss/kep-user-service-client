import KepUserServiceClient from '../dist/esm/index.js';
import config from './config.json';
import inspect from 'object-inspect';

const test = async () => {
    const service = new KepUserServiceClient(
        config.user,
        config.pass,
        config.overrides,
    );

    
    console.log(
        inspect(
            await service.getEmployee("051655852"),
            { depth: 10 },
        ),
    );

    console.log(
        inspect(
            await service.getAllEmployees("0699"),
            { depth: 10 },
        ),
    );
};

test();
