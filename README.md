# kep-user-service-client

Client to connect on KEP Employee registry service.

Returns KEP employee details by AFM and all employees of a KEP by kepcode.

#### Methods
all methods of kep-user service covered by this client:
* getEmployee
* getAllEmployees

#### Example

```
import KepUserServiceClient from '@digigov-oss/kep-user-service-client';
import config from './config.json'; 
const test = async () => {
    const client = new KepUserServiceClient(config.user, config.pass);
    const afm = '012345678';
    try {
        const kedResponse = await client.getEmployee(afm)
        console.log(kedResponse);
    } catch (error) {
        console.log(error);
    }
}
test();

```

```
import KepUserServiceClient from '@digigov-oss/kep-user-service-client';
import config from './config.json'; 
const test = async () => {
    const client = new KepUserServiceClient(config.user, config.pass);
    const kepcode = '0699';
    try {
        const kedResponse = await client.getAllEmployees(kepcode)
        console.log(kedResponse);
    } catch (error) {
        console.log(error);
    }
}
test();

```