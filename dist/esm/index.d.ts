import { AuditRecord, AuditEngine } from '@digigov-oss/gsis-audit-record-db';
import { GetKepUserRegAllEmployeesResponse, GetKepUserRegEmployeeResponse, KedResponseWithAuditRecord } from './interface';
export * from './interface';
/**
 * @type Overrides
 * @description Overrides for the REST client
 * @param {string} endpoint - Endpoint to be used for the REST client
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 * @param {AuditEngine} auditEngine - Audit engine to be used for the audit record produced
 */
export type Overrides = {
    endpoint?: string;
    prod?: boolean;
    auditInit?: AuditRecord;
    auditStoragePath?: string;
    auditEngine?: AuditEngine;
};
/**
 *
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 */
declare class KepUserServiceClient {
    endpoint: string;
    prod: boolean;
    auditInit: object;
    auditStoragePath: string;
    auditEngine: AuditEngine;
    user: string;
    pass: string;
    constructor(user: string, pass: string, overrides?: Overrides);
    genAuditRecord: (overrides?: Overrides) => Promise<AuditRecord>;
    getEmployee: (afm: string, overrides?: Overrides) => Promise<KedResponseWithAuditRecord<GetKepUserRegEmployeeResponse>>;
    getAllEmployees: (kepcode: string, overrides?: Overrides) => Promise<KedResponseWithAuditRecord<GetKepUserRegAllEmployeesResponse>>;
}
export default KepUserServiceClient;
