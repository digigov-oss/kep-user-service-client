import { generateAuditRecord, FileEngine, } from '@digigov-oss/gsis-audit-record-db';
import { RestClient } from './restClient.js';
import config from './config.json';
export * from './interface';
/**
 *
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 */
class KepUserServiceClient {
    endpoint;
    prod;
    auditInit;
    auditStoragePath;
    auditEngine;
    user;
    pass;
    constructor(user, pass, overrides) {
        this.prod = overrides?.prod ?? false;
        if (overrides?.endpoint) {
            this.endpoint = overrides?.endpoint;
        }
        else {
            this.endpoint = this.prod ? config.prod : config.test;
        }
        this.auditInit = overrides?.auditInit ?? {};
        this.auditStoragePath = overrides?.auditStoragePath ?? '/tmp';
        this.auditEngine =
            overrides?.auditEngine ?? new FileEngine(this.auditStoragePath);
        this.user = user;
        this.pass = pass;
    }
    genAuditRecord = async (overrides) => {
        const auditInit = Object.assign({}, this.auditInit, overrides?.auditInit);
        const auditRecord = await generateAuditRecord(auditInit, this.auditEngine);
        if (!auditRecord)
            throw new Error('Audit record is not initialized');
        return auditRecord;
    };
    getEmployee = async (afm, overrides) => {
        const auditRecord = await this.genAuditRecord(overrides);
        const restClient = new RestClient(this.user, this.pass, this.endpoint);
        const resp = await restClient.getEmployee(afm, auditRecord);
        return {
            kedResponse: resp,
            auditRecord: auditRecord,
        };
    };
    getAllEmployees = async (kepcode, overrides) => {
        const auditRecord = await this.genAuditRecord(overrides);
        const restClient = new RestClient(this.user, this.pass, this.endpoint);
        const resp = await restClient.getAllEmployees(kepcode, auditRecord);
        return {
            kedResponse: resp,
            auditRecord: auditRecord,
        };
    };
}
export default KepUserServiceClient;
