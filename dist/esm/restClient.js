import axios from 'axios';
export class RestClient {
    user;
    pass;
    endpoint;
    constructor(user, pass, endpoint) {
        this.user = user;
        this.pass = pass;
        this.endpoint = endpoint;
    }
    getEmployee = async (afm, auditRecord) => {
        const request = {
            auditRecord: auditRecord,
            getKepUserRegEmployeeInputRecord: {
                afm,
            },
        };
        const options = {
            auth: {
                username: this.user,
                password: this.pass,
            },
        };
        const ed = `${this.endpoint}/getKepUserRegEmployee`;
        const response = await axios.post(ed, request, options);
        return response.data;
    };
    getAllEmployees = async (kepCode, auditRecord) => {
        const request = {
            auditRecord: auditRecord,
            getKepUserRegAllEmployeesInputRecord: {
                kepCode,
            },
        };
        const options = {
            auth: {
                username: this.user,
                password: this.pass,
            },
        };
        const ed = `${this.endpoint}/getKepUserRegAllEmployees`;
        const response = await axios.post(ed, request, options);
        return response.data;
    };
}
