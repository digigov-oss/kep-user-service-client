import { AuditRecord } from '@digigov-oss/gsis-audit-record-db';
export type ErrorRecord = {
    errorCode: string;
    errorDescr: string;
};
export type KepEmployee = {
    afm: string;
    ypalEmail: string;
    kepCode: string;
    kepCodeApasx: string;
    ekpaideysi: string;
    idiotita: string;
    etos: string;
    dateLixi: string | null;
    name: string;
    surname: string;
    ypalKlados: string;
    dateMetavolis: string;
    allaKeps: string[];
};
export type KepEmployeeList = {
    employees: KepEmployee[];
};
export interface KedResponseWithAuditRecord<T> {
    kedResponse: T;
    auditRecord: AuditRecord;
}
export type GetKepUserRegEmployeeRequest = {
    auditRecord: AuditRecord;
    getKepUserRegEmployeeInputRecord: {
        afm: string;
    };
};
export type GetKepUserRegEmployeeResponse = {
    getKepUserRegEmployeeOutputRecord: {
        employee: KepEmployee;
    };
    callSequenceId: string;
    callSequenceDate: string;
    errorRecord?: ErrorRecord;
};
export type GetKepUserRegAllEmployeesRequest = {
    auditRecord: AuditRecord;
    getKepUserRegAllEmployeesInputRecord: {
        kepCode: string;
    };
};
export type GetKepUserRegAllEmployeesResponse = {
    getKepUserRegAllEmployeesOutputRecord: {
        employees: KepEmployee[];
    };
    callSequenceId: string;
    callSequenceDate: string;
    errorRecord?: ErrorRecord;
};
