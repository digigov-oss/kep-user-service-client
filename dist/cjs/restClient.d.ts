import { AuditRecord } from '@digigov-oss/gsis-audit-record-db';
import { GetKepUserRegEmployeeResponse, GetKepUserRegAllEmployeesResponse } from './interface';
export declare class RestClient {
    user: string;
    pass: string;
    endpoint: string;
    constructor(user: string, pass: string, endpoint: string);
    getEmployee: (afm: string, auditRecord: AuditRecord) => Promise<GetKepUserRegEmployeeResponse>;
    getAllEmployees: (kepCode: string, auditRecord: AuditRecord) => Promise<GetKepUserRegAllEmployeesResponse>;
}
