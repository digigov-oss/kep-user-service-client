"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const gsis_audit_record_db_1 = require("@digigov-oss/gsis-audit-record-db");
const restClient_js_1 = require("./restClient.js");
const config_json_1 = __importDefault(require("./config.json"));
__exportStar(require("./interface"), exports);
/**
 *
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 */
class KepUserServiceClient {
    constructor(user, pass, overrides) {
        var _a, _b, _c, _d;
        this.genAuditRecord = (overrides) => __awaiter(this, void 0, void 0, function* () {
            const auditInit = Object.assign({}, this.auditInit, overrides === null || overrides === void 0 ? void 0 : overrides.auditInit);
            const auditRecord = yield (0, gsis_audit_record_db_1.generateAuditRecord)(auditInit, this.auditEngine);
            if (!auditRecord)
                throw new Error('Audit record is not initialized');
            return auditRecord;
        });
        this.getEmployee = (afm, overrides) => __awaiter(this, void 0, void 0, function* () {
            const auditRecord = yield this.genAuditRecord(overrides);
            const restClient = new restClient_js_1.RestClient(this.user, this.pass, this.endpoint);
            const resp = yield restClient.getEmployee(afm, auditRecord);
            return {
                kedResponse: resp,
                auditRecord: auditRecord,
            };
        });
        this.getAllEmployees = (kepcode, overrides) => __awaiter(this, void 0, void 0, function* () {
            const auditRecord = yield this.genAuditRecord(overrides);
            const restClient = new restClient_js_1.RestClient(this.user, this.pass, this.endpoint);
            const resp = yield restClient.getAllEmployees(kepcode, auditRecord);
            return {
                kedResponse: resp,
                auditRecord: auditRecord,
            };
        });
        this.prod = (_a = overrides === null || overrides === void 0 ? void 0 : overrides.prod) !== null && _a !== void 0 ? _a : false;
        if (overrides === null || overrides === void 0 ? void 0 : overrides.endpoint) {
            this.endpoint = overrides === null || overrides === void 0 ? void 0 : overrides.endpoint;
        }
        else {
            this.endpoint = this.prod ? config_json_1.default.prod : config_json_1.default.test;
        }
        this.auditInit = (_b = overrides === null || overrides === void 0 ? void 0 : overrides.auditInit) !== null && _b !== void 0 ? _b : {};
        this.auditStoragePath = (_c = overrides === null || overrides === void 0 ? void 0 : overrides.auditStoragePath) !== null && _c !== void 0 ? _c : '/tmp';
        this.auditEngine =
            (_d = overrides === null || overrides === void 0 ? void 0 : overrides.auditEngine) !== null && _d !== void 0 ? _d : new gsis_audit_record_db_1.FileEngine(this.auditStoragePath);
        this.user = user;
        this.pass = pass;
    }
}
exports.default = KepUserServiceClient;
